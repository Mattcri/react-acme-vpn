import React from "react";
import { Carousel } from "react-bootstrap";
import { Container, Row, Col } from "react-bootstrap";
import hero1 from "../img/hero-about.jpg"
import hero2 from "../img/hero-about.jpg"
import hero4 from "../img/hero4.jpg"
import hero5 from "../img/hero5.jpg"
import hero6 from "../img/hero6.jpg"




const About = () => {
  return (
    <section className="carousel-hero">
      <Carousel fade indicators={false} interval={6000}>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={hero1}
            alt="First slide"
          />
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={hero2}
            alt="Second slide"
          />
        </Carousel.Item>
      </Carousel>

      <main className="pt-4 pt-lg-5">
        <section className="register py-3 py-md-4 mb-lg-4">
          <Container>
            <Row xs={1} md={2} className="align-items-center">
              <Col className="text-center mb-5 mb-md-0">
                <h1>What's a VPN?</h1>
                <h4>VPN stands for “virtual private network” — a service that protects your internet connection and privacy online. It creates an encrypted tunnel for your data, protects your online identity by hiding your IP address, and allows you to use public Wi-Fi hotspots safely.</h4>
                <h1>Why use ACME-VPN?</h1>
                <h4>ACME VPN comes bundled with the latest and greatest technology and features for privacy and anonimity at a price you can afford. Our desktop and mobile applications are fully open source and have been audited by independent experts.
ACME VPN provides 24/7 customer support and a 90-day money back guarantee, so you can cancel your subscription anytime if you're not satisfied.</h4>
              </Col>

            </Row>
          </Container>
        </section>
        
        <section className="carousel-hero">
          <Carousel fade indicators={false} interval={6000}>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={hero4}
              alt="First slide"
            />
            <Carousel.Caption>
              <h2>Protect yourself from malicious hackerz</h2>
              <p>ACME VPN encrypts your online traffic and protects you from dangerous Man-in-the-Middle attacks.</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={hero5}
              alt="Second slide"
            />

            <Carousel.Caption>
              <h2>Unlock World-Wide Content</h2>
              <p>Use ACME VPN to access REGION-LOCKED content in other countries!</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={hero6}
              alt="third slide"
            />

            <Carousel.Caption>
              <h2>A very strict no log policy</h2>
              <p>We do not store any private data of our customers, not even their IP addresses! Honest! this is NOT a CIA honeypot!</p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>

        </section>

        <section className="features-vpn py-4 py-lg-5">
          <Container>
            <h2 className="text-center mb-2">Still unsure about VPNs? Watch this video! <br></br> </h2>
          </Container>
        </section>

      </main>
    </section>
  )
}

export default About