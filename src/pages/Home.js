import React from "react";
import { Carousel } from "react-bootstrap";
import { Container, Row, Col, Form, Button, Card } from "react-bootstrap";
import hero1 from "../img/hero-1.jpg"
import hero2 from "../img/hero-3.jpg"
import feature1 from "../img/feature-1.svg"
import feature2 from "../img/feature-2.svg"
import feature3 from "../img/feature-3.svg"

const Home = () => {
  return (
    <section className="carousel-hero">
      <Carousel fade indicators={false} interval={6000}>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={hero1}
            alt="First slide"
          />
          <Carousel.Caption>
            <h2>Protect your Devices</h2>
            <p>Our service allows you to protect an unlimited number of devices.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={hero2}
            alt="Second slide"
          />

          <Carousel.Caption>
            <h2>Two-factor Authentication</h2>
            <p>The safest way to protect your accesses.</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>

      <main className="pt-4 pt-lg-5">
        <section className="register py-3 py-md-4 mb-lg-4">
          <Container>
            <Row xs={1} md={2} className="align-items-center">
              <Col className="text-center mb-5 mb-md-0">
                <h1>Protect yourself from online trackers and malicious actors</h1>
                <h4>with ACME-VPN's military-grade end-to-end encryption and secure your online activity</h4>
              </Col>
              <Col className="px-xl-5">
                <Form className="px-3 px-lg-4 py-3 py-lg-4">
                  <Row className="mb-3">
                    <Form.Group as={Col} controlId="formFirstName">
                      <Form.Label>First Name</Form.Label>
                      <Form.Control type="text" placeholder="Your name" />
                    </Form.Group>

                    <Form.Group as={Col} controlId="formLastName">
                      <Form.Label>Last Name</Form.Label>
                      <Form.Control type="text" placeholder="Your last name" />
                    </Form.Group>
                  </Row>

                  <Form.Group className="mb-3" controlId="formEmail">
                    <Form.Label>Email</Form.Label>
                    <Form.Control placeholder="name@example.com" />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="formGridAddress">
                    <Form.Label>Address</Form.Label>
                    <Form.Control placeholder="Apartment, studio, or floor" />
                  </Form.Group>

                  <Form.Group className="mb-4" controlId="formPhone">
                    <Form.Label>Phone</Form.Label>
                    <Form.Control placeholder="" />
                  </Form.Group>

                  <Form.Group className="text-center">
                    <Button variant="primary" type="submit" >
                      Submit
                    </Button>
                  </Form.Group>

                </Form>
              </Col>

            </Row>
          </Container>
        </section>

        <section className="features-vpn py-4 py-lg-5">
          <Container>
            <h2 className="text-center mb-2">Shield your cybersecurity <br></br> with the help of Threat Protection</h2>
            <Row xs={1} md={2} lg={4} className="justify-content-center align-items-stretch py-3 py-lg-5">
              <Col className="mb-3 mb-lg-0">
                <Card className="text-center">
                  <Card.Img variant="top" src={feature1} />
                  <Card.Body>
                    <Card.Title>Card Title</Card.Title>
                    <Card.Text>
                      Some quick example text to build on the card title and make up the
                      bulk of the card's content.
                    </Card.Text>
                    <Button variant="danger">Go somewhere</Button>
                  </Card.Body>
                </Card>
              </Col>

              <Col>
                <Card className="text-center">
                  <Card.Img variant="top" src={feature2} />
                  <Card.Body>
                    <Card.Title>Card Title</Card.Title>
                    <Card.Text>
                      Some quick example text to build on the card title and make up the
                      bulk of the card's content.
                    </Card.Text>
                    <Button variant="danger">Go somewhere</Button>
                  </Card.Body>
                </Card>
              </Col>

              <Col>
                <Card className="text-center">
                  <Card.Img variant="top" src={feature3} />
                  <Card.Body>
                    <Card.Title>Card Title</Card.Title>
                    <Card.Text>
                      Some quick example text to build on the card title and make up the
                      bulk of the card's content.
                    </Card.Text>
                    <Button variant="danger">Go somewhere</Button>
                  </Card.Body>
                </Card>
              </Col>

            </Row>
          </Container>
        </section>

      </main>
    </section>
  )
}

export default Home