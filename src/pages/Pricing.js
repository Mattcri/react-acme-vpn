import React from "react";
import { Container, Row, Col, Card, Accordion } from "react-bootstrap"
import heroPricing from "../img/hero-pricing.jpg"

const Pricing = () => {
  return(
    <section>
      <Container fluid className="px-0 hero-pricing">
        <img src={heroPricing} alt="Hero Pricing"></img>
      </Container>

      <main className="prices-acme-vpn">
        <Container className="py-4 py-md-5">
          <h1 className="text-center mb-4">Our Prices</h1>
          <p className="text-center h4">Choose the plan that suits your needs</p>
          <Row xs={1} md={2} lg={3} className="justify-content-center align-items-stretch py-3 py-lg-4">
            <Col>
              <Card border="primary" className="mb-3 h-100">
                <Card.Header className="bg-transparent border-primary fw-bolder">Basic Plan</Card.Header>
                <Card.Body>
                  <Card.Title className="text-success">$3.99</Card.Title>
                  <ul>
                    <li className="mb-1">Secure and high-speed VPN</li>
                    <li className="mb-1">Malware protection</li>
                    <li className="mb-1">Tracker and ad blocker</li>
                  </ul>
                </Card.Body>
              </Card>
            </Col>

            <Col>
              <Card border="primary" className="mb-3 h-100">
                <Card.Header className="bg-transparent border-primary fw-bolder">Premiun Plan</Card.Header>
                <Card.Body>
                  <Card.Title className="text-success">$4.55</Card.Title>
                  <ul>
                    <li className="mb-1">All basic and intermediate plan features</li>
                    <li className="mb-1">1TB of encrypted cloud storage</li>
                  </ul>
                </Card.Body>
              </Card>
            </Col>

            <Col>
              <Card border="primary" className="mb-3 h-100">
                <Card.Header className="bg-transparent border-primary fw-bolder">Intermediate Plan</Card.Header>
                <Card.Body>
                  <Card.Title className="text-success">$5.25</Card.Title>
                  <ul>
                    <li className="mb-1">All basic plan features</li>
                    <li className="mb-1">Cross-platform password manager</li>
                    <li className="mb-1">Data breach scanner</li>
                  </ul>
                </Card.Body>
              </Card>
            </Col>

          </Row>

          <Row className="pt-5 pb-4 justify-content-center">
            <Col xs={12} className="mb-4">
              <h2 className="text-center">FAQ</h2>
            </Col>
            <Col xs={12} lg={9} xl={7}>
              <Accordion>
                <Accordion.Item eventKey="0">
                  <Accordion.Header>What is your money-back policy?</Accordion.Header>
                  <Accordion.Body>
                    <p>Here is the refund and cancellation policy at the core of our 30-day money-back guarantee:</p>
                    
                    <p className="italic">"If you wish to claim a refund, you can do so within 30 days following your purchase of our Services. We do not grant refunds for recurring subscription payments if you cancel the Services after the renewal unless applicable law provides otherwise."</p>

                    <p>This and other conditions of our service can be found in our Terms of Service.
                      Note: Please bear in mind, that we cannot issue refunds for the purchases made through Apple's App Store. However, you can contact App Store support for a refund.</p>
                  </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="1">
                  <Accordion.Header>How many devices can I use</Accordion.Header>
                  <Accordion.Body>
                    <p>A total of six devices can be connected with one NordVPN account at the same time. However, there is one condition. If you connect the devices to the same server, you have to choose different VPN protocols (TCP and UDP are different protocols, so you can connect one device to TCP and another to UDP on the same server). That means that a total of five devices can be connected to one server at the same time: through HTTP proxy, SOCKS5, NordLynx, OpenVPN TCP, and OpenVPN UDP. If you have one more device, you can connect it to another server using any protocol.</p>
                    <p>
                      If you have even more devices, you can set up your router with our VPN. The router uses only one device slot, but all the devices connected to the network will be under VPN protection.
                    </p>
                  </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="2">
                  <Accordion.Header>How can I delete my account?</Accordion.Header>
                  <Accordion.Body>
                    <p>To request the removal of an account, fill in the form below.</p>
                    <p className="italic">
                      If you do not see the form below, try disabling your ad-blocking extension/software for our website. Ad-blockers are usually the culprits in such scenarios. Alternatively, you can contact our support team.
                    </p>
                  </Accordion.Body>
                </Accordion.Item>
              </Accordion>
            </Col>
          </Row>

        </Container>
      </main>
    </section>
  )
}

export default Pricing