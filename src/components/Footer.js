import React from "react";
import { Container } from "react-bootstrap";

const Footer = () => {
  return (
    <footer className="bg-primary ">
      <Container>
        <p>© ACME-VPN 2022</p>
      </Container>
    </footer>
  )
}

export default Footer