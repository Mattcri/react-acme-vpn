import React from "react";
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { Link } from 'react-router-dom'

const Header = () => {
  return (
    <header className="sticky-top">
      <Navbar bg="light" expand="lg" className="py-3">
        <Container>
          <Navbar.Brand to="/" className="text-primary font-weigth fw-bold">ACME VPN</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto ms-auto mb-2 mb-lg-0">
              <Nav.Link as={Link} to="/"> Home </Nav.Link>
              <Nav.Link as={Link} to="/pricing"> Pricing </Nav.Link>
              <Nav.Link as={Link} to="/about"> Why usea a VPN </Nav.Link>
              <NavDropdown title="Downloads" id="basic-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">Some</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">
                  Another action
                </NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">
                  Separated link
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
            <div className="d-flex">
              <button className="btn btn-outline-primary me-3 rounded-pill" data-bs-toggle="modal" data-bs-target="#signIn">Sign in</button>
              <button className="btn btn-outline-primary rounded-pill" data-bs-toggle="modal" data-bs-target="#signUp">Sign up</button>
            </div>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </header>
    
  )
}

export default Header